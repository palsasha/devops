yc compute instance create \
  --name itmo-test-1 \
  --zone ru-central1-a \
  --network-interface subnet-name=test1-ru-central1-a,nat-ip-version=ipv4 \
  --create-boot-disk image-folder-id=standard-images,image-family=ubuntu-2004-lts \
  --ssh-key ~/.ssh/id_rsa.pub
